{{ Form::open(['route' => 'session.store']) }}
	
	<div class="form-group">
		{{ Form::label('email', "Email:") }}
		{{ Form::text('email', null) }}
	</div>

	<div class="form-group">
		{{ Form::label('password', "Password:") }}
		{{ Form::password('password') }}
	</div>

	<div class="form-group">
		{{ Form::submit('Log in') }}
	</div>

{{ Form::close() }}