<h2>Create Tweet</h2>

{{ Form::open(['route' => 'tweets.store'])}}
	<div>
		{{ Form::label('text', 'Text') }}
		{{ Form::textarea('text', null) }}
		{{ $errors->first('text') }}
	</div>

	<div>
		{{ Form::submit('Create Text') }}
	</div>
{{ Form::close() }}