<h2>Create Tweet</h2>

{{ Form::model($tweet, ['method' => 'PATCH', 'route' => ['tweets.update', $tweet->id]])}}
	<div>
		{{ Form::label('text', 'Text') }}
		{{ Form::textarea('text', null) }}
		{{ $errors->first('text') }}
	</div>

	<div>
		{{ Form::submit('Edit Text') }}
	</div>
{{ Form::close() }}