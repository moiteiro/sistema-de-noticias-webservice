<h2>Edit User</h2>

{{ Form::model($user, ['method' => 'PATCH', 'route' => ['users.update', $user->id]]) }}
	<div>
		{{ Form::label('name', 'Name') }}
		{{ Form::text('name', null) }}
		{{ $errors->first('name') }}
	</div>

	<div>
		{{ Form::label('username', 'Username') }}
		{{ Form::text('username', null) }}
		{{ $errors->first('username') }}
	</div>

	<div>
		{{ Form::label('email', 'Email') }}
		{{ Form::text('email', null) }}
		{{ $errors->first('email') }}
	</div>

	<div>
		{{ Form::label('password', 'Password') }}
		{{ Form::password('password', null) }}
		{{ $errors->first('password') }}
	</div>

	<div>
		{{ Form::submit('Edit User') }}
	</div>
{{ Form::close() }}
