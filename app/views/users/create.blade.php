<h2>Create User</h2>

{{ Form::open(['route' => 'users.store'])}}
	<div>
		{{ Form::label('name', 'Name') }}
		{{ Form::text('name', null) }}
		{{ $errors->first('name') }}
	</div>

	<div>
		{{ Form::label('username', 'Username') }}
		{{ Form::text('username', null) }}
		{{ $errors->first('username') }}
	</div>

	<div>
		{{ Form::label('email', 'Email') }}
		{{ Form::text('email', null) }}
		{{ $errors->first('email') }}
	</div>

	<div>
		{{ Form::label('password', 'Password') }}
		{{ Form::password('password', null) }}
		{{ $errors->first('password') }}
	</div>

	<div>
		{{ Form::submit('Create User') }}
	</div>
{{ Form::close() }}
