<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Sistema de Noticias</title>
	{{ HTML::script('http://code.jquery.com/jquery-2.1.1.js') }}
	<style>
		@import url(//fonts.googleapis.com/css?family=Lato:400,700,400italic);

		body {
			margin:0;
			font-family:'Lato', sans-serif;
			padding: 10px;
		}

		.welcome {
			color: #242a30;
			width: 300px;
			height: 200px;
			margin: 0 auto;
			text-align:center;
		}

		a, a:visited {
			text-decoration:none;
		}

		h1 {
			font-size: 32px;
			margin: 16px 0 0 0;
		}

		h1, h2 {
			font-weight: 700;
		}

		li{
			margin: 10px 0;
		}

		.request {
			color: #3b73af;
			font-style: italic;
		}

		.table {
			width: 100%;
			border: 1px solid #242a30;
			margin-bottom: 20px;
			border-radius: 3px;
			background: white;
		}

		.table th {
			text-align: left;
			border-bottom: 2px solid #242a30;
			color: #242a30;
			font-weight: 700;
			padding: 10px 15px;
		}

		.table td {
			padding: 10px 15px;
			line-height: 1.42857143;
			border-bottom: 1px solid #e2e7eb;
		}

		.table tr:last-child td {
			border: none;
		}

	</style>
</head>
<body>
	<div class="welcome">
		<h1>Sistema de Not&iacute;cias Web Service</h1>
	</div>

	<div >
		<h3>Tabela de Respostas</h3>
		<table class='table'>
			<thead>
				<tr>
					<th>Status</th>
					<th>C&oacute;digo</th>
					<th>Mensagem</th>
				</tr>
			</thead>

			<tbody>
				<tr>
					<td>401</td>
					<td>21</td>
					<td>Tweet n&atilde;o pertence ao usu&aacute;rio</td>
				</tr>
				<tr>
					<td>404</td>
					<td>--</td>
					<td>Recurso n&atilde;o encontrado</td>
				</tr>
				<tr>
					<td>422</td>
					<td>31</td>
					<td>Erro de valida&ccedil;&atilde;o</td>
				</tr>
			</tbody>
		</table>
	</div>
	
	<br>
	<br>
	
	<h2>Patch Note:</h2>
	<br>
	
	<div>
		<h3>Vers&atilde;o 1.0.2:</h3>
		<ul>
			<li>
				<b>[Response]</b> Todos os erros <b>400's</b> est&atilde;o sendo acompanhados de um c&oacute;digo e um texto informativo.
			</li>
			<li>
				<b>[Validation]</b> Todos os recursos est&atilde;o sendo validados. O erro valida&ccedil;&atilde;o &eacute; retornado como um <b>422</b> informando quais campos foram invalidados e com um texto do motivo.
			</li>

			<li>
				<b>[Tweets]</b> A listagem de tweets agora os retorna em ordem decrescente de cria&ccedil;&atilde;o/atualiza&ccedil;&atilde;o.
			</li>

			<li>
				<b>[Tweets]</b> Todos os tweets de um determinado usu&aacute;rio podem ser retornados atrav&eacute;s de:
				<br>
				<span class='request'>GET /users/{user_id}/tweets </span>
			</li>
		</ul>
	</div>

		
	<br>
	<br>

	<div>
		<h3>Vers&atilde;o 1.0.1:</h3>
		<ul>
			<li>
				<b>[Database]</b> Tivermos que dar um rollback no database para adicionar integridade aos dados relacionados aos <b>Tweets</b> e <b>Usuários</b>
			</li>
			<li>
				<b>[Response]</b> Agora uma nova resposta do tipo <b>401</b> será enviada caso o usuário não posso realizar alguma operação.
				<br><br>
				Por exemplo, se o usuário tentar criar um tweet sem estar logado, ele receberá um <b>401</b>.
				<br>
				Se o usuário tentar editar ou remover um tweet que não eh de sua autoria, ele também receberá um <b>401</b>.
			</li>
			<li>
				<b>[Tweets]</b> Consertamos uns problemas na edição e remoção dos tweets que não estavam corretamente implementados.
			</li>
			<li>
				<b>[Tweets]</b> Não é mais necessário enviar o id do <b>Usuário</b> quando for criar um <b>tweet</b>. O id agora é pêgo através da sessão.
			</li>
			<li>
				<b>[Session]</b> para iniciar uma sessão, a aplicação precisa enviar uma requisição do tipo POST para /session com o email e a senha do usuário. 
				A aplicação receberá {user} ou false como resposta
				<br>
				<span class='request'>POST /session {email, password}</span>
			</li>
		</ul>
	</div>
</body>
</html>
