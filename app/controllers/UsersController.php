<?php

class UsersController extends \BaseController {

	public function __construct() 
	{
		$this->beforeFilter('auth', ['only' => ['edit', 'update', 'destroy']]);
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return User::all();
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('users.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$validation = Validator::make(Input::all(), ['name'=>'required','username'=>'required|Unique:users','name'=>'required','email'=>'required|Email','password'=>'required']);

		if($validation->fails())
		{
		    $error = [
				'code' => '31', 
				'message' => $validation->messages()->toArray()
				];
			return Response::json($error, 422);
		}

	
		$input = Input::all();


			$user = User::create([
				'name' => $input['name'],
				'username' => $input['username'],
				'email' => $input['email'],
				'password' => $input['password'],
			]);
		}
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return User::findOrFail($id);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = User::findOrFail($id);

		if (Auth::user() == $user)
			return View::make('users.edit', compact('user'));
		return Response::make('Unauthorized', 401);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$user = User::findOrFail($id);

		if (Auth::user() == $user)
		{
			$input = Input::all();

			$user->name = $input['name'];
			$user->username = $input['username'];
			$user->email = $input['email'];

			if ( $input['password'] )
				$user->password = $input['password'];
				
			return $user->save() ? Response::json(['result' => 'true']) : Response::json(['result' => 'false']);
		}

		return Response::make('Unauthorized', 401);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{

		$user = User::findOrFail($id);

		if (User::user() == $user)
		{
			return $user->delete() ? Response::json(['result' => 'true']) : Response::json(['result' => 'false']);
		}

		return Response::make('Unauthorized', 401);
	}


}
