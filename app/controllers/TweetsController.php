<?php

class TweetsController extends \BaseController {


	public function __construct() 
	{
		$this->beforeFilter('auth', ['only' => ['create', 'store', 'update', 'edit', 'delete']]);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return Tweet::orderBy('updated_at', 'desc')->get();
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('tweets.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
	
		$validation = Validator::make(Input::all(), ['text'=>'required|Max:200']);

		if($validation->fails())
		{
			$error = [
				'code' => '31', 
				'message' => $validation->messages()->toArray()
				];
			return Response::json($error, 422);
		}

		$input = Input::all();
		$tweet = new Tweet();
		$tweet->text = $input['text'];

		return Auth::user()->tweets()->save($tweet);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$tweet = Tweet::find($id);

		if ($tweet)
			return $tweet;

		return Response::make('Not Found', 404);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function showByUser($user_id)
	{
		$user = User::find($user_id);

		if ($user)
			return Tweet::whereUserId($user_id)->orderBy('updated_at', 'desc')->get();

		return Response::make('Not Found', 404);	
	}



	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$tweet = Tweet::findOrFail($id);

		if (Auth::user()->id == $tweet->user_id)
		{
			return View::make('tweets.edit', compact('tweet'));
		}
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$tweet = Tweet::findOrFail($id);

		if (Auth::user()->id == $tweet->user_id)
		{
			$input = Input::only('text');

			$tweet->fill($input)->save();

			if ($tweet->save()) 
			{
				return Response::json(['result' => 'true']);
			}

			return Response::json(['result' => 'false']);
		}

		else 
		{
			return Response::json(['code' => '21', 'message' => 'not belongs to this user'], 401);
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$tweet = Tweet::findOrFail($id);

		if (Auth::user()->id == $tweet->user_id)
		{
			if ($tweet->delete()) 
			{
				return Response::json(['result' => 'true']);
			}
			return Response::json(['result' => 'false']);
		}

		else 
		{
			return Response::json(['code' => '21', 'message' => 'not belongs to this user'], 401);
		}
	}


}
