<?php 

class Tweet extends Eloquent {
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tweets';

	protected $fillable = ['text', 'author'];

	public function user() {
		$this->belongsTo('User');
	}
}